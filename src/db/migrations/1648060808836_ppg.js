exports.up = (knex) => {
    return knex.schema.createTable('ppgs', (table) => {
        table.increments('id').primary();
        table.timestamps(true, true);
        table.text('ppg', 'longtext');
        table.text('ppg_alt', 'longtext');
        table.text('ecg', 'longtext');
        table.integer('bp_systolic');
        table.integer('bp_diastolic')
        table.float('true_gluco');
        table.integer('subject_age');
        table.float('subject_height');
        table.float('subject_weight');
        table.string('subject_sex');
        table.boolean('subject_isdiabetic');
        table.string('subject_email');
        table.float('dim_corr_ppg');
        table.float('hurst_exp_ppg');
        table.float('lyapunov_exp_ppg');
        table.text('attractor_ppg', 'longtext');
        table.float('dim_corr_alt');
        table.float('hurst_exp_alt');
        table.float('lyapunov_exp_alt');
        table.text('attractor_alt', 'longtext');
        table.float('dim_corr_ecg');
        table.float('hurst_exp_ecg');
        table.float('lyapunov_exp_ecg');
        table.text('attractor_ecg', 'longtext');
        table.integer('processing_status_ppg').defaultTo(0);
        table.integer('processing_status_alt').defaultTo(0);
        table.integer('processing_status_ecg').defaultTo(0);
    });
};

exports.down = (knex) => {
    return knex.schema.dropTableIfExists('ppgs');
};
