const {Model} = require('objection');

class Ppg extends Model{
    static get tableName(){
        return 'ppgs';
    }

    static get relationMappings(){
    }
}

module.exports = Ppg;
