import pandas as pd
from scipy import signal
import matplotlib.pyplot as plt
import numpy as np
import math
import sys
import json
from numpy import linalg as LA
from io import StringIO

def filterTimeSeries(timeSeries, n, Rp, Wp, Ts):
    #n -> orden
    #Rp -> ripple window [dB]
    #Wp -> frecuencia de corte [Hz]
    #Ts -> Periodo de muestreo [ms]
    Wn = Wp*2*Ts/1000
    
    #scipy.signal.cheby1(N, rp, Wn, btype='low', analog=False, output='ba', fs=None)
    b, a = signal.cheby1(n, Rp, Wn, 'lowpass')
    filteredTimeSeries = signal.filtfilt(b, a, timeSeries,axis=0)
    
    return filteredTimeSeries
    
def plotPPGSeries(timeSeries):
    fig = plt.figure(figsize = (5, 2)) 
    ax = fig.add_axes([-0.3, -0.3, 2, 1])
    t = np.linspace(0, 20, 2000)
    ax.plot(t, timeSeries) 
    fig.suptitle('PPG') 
    plt.show()
    
def pim(series,taumax,partitions):
    series = (series-min(series))/(max(series)-min(series))
    N = len(series)
    PIM = ami(series,range(1,taumax+1),partitions,N)

    if (primin(PIM,N) == 0):
        TauOpt = taumax
    else:
        TauOpt = primin(PIM,N)
        
    return PIM, TauOpt

def ami(x,tau,k,N):
    mutua = []
    # I(x_n,x_{n+\\tau})=pim(x,tau,KHistograma)
    for i in range(0,len(tau)):
        for k1 in range(1,k+1):
            for k2 in range(1,k+1):
                # Histogramas unidimensionales                    
                px = np.where(((k1-1)/k < x[0:N-tau[i]]) & (x[0:N-tau[i]] <= k1/k))
                
                py = np.where(((k2-1)/k < x[tau[i]:N]) & (x[tau[i]:N] <= k2/k))

                # Histograma bidimensional
                Ixy = np.where(((k1-1)/k < x[0:N-tau[i]]) & (x[0:N-tau[i]] <= k1/k) & ((k2-1)/k < x[tau[i]:N]) & (x[tau[i]:N] <= k2/k))
                    
                Ixy = (Ixy)[0].size
                Pxy = Ixy
                # Densidades de probabilidad
                if (Pxy > 0):
                    Px = (px)[0].size/(N-tau[i])
                    Py = (py)[0].size/(N-tau[i])
                    Pxy = Pxy/(N-tau[i])
                    # Promedio de informacion mutua
                    mutua.append(Pxy * math.log(Pxy/(Px * Py),2))
    return mutua

def primin(X,N):
    # Devuelve el primer minimo de I(x_n,x_{n+T})
    T = 0
    for m in range(3, N + 1):
        derivp1 = X[m-2] - X[m-3]
        derivp2 = X[m-1] - X[m-2]
        if ((derivp1 < 0) and (derivp2 > 0)):
            T = m-2;
            return T
    return T

def knn_deneme(x, tao, mmax, rtol, atol):
    # x : time series
    # tao : time delay
    # mmax : maximum embedding dimension
    # reference:M. B. Kennel, R. Brown, and H. D. I. Abarbanel, Determining
    # embedding dimension for phase-space reconstruction using a geometrical 
    # construction, Phys. Rev. A 45, 3403 (1992).

    N = len(x)
    
    # Standard deviation
    Ra = np.std(x, ddof = 0)
    emb_dim = 0
    
    FNN = np.zeros((mmax,1))
    # Compute the FNN with each m iteration
    for m in range(1, mmax+1):
        M = N - m*tao
        
        # Reconstruct the attractor
        Y = psr_deneme(x, m, tao, M)
        
        # Each iteration calculates the distance from one point in the attractor
        # with other point in the space state
        for n in range(1, M+1):
            y0 = np.ones([M,1]) * Y[n-1,:]
            # Obtaining the distance vector
            distance = np.sqrt(np.sum((Y-y0)**2,1))
            # Sorting the distance vector
            neardis = np.sort(distance,axis=0)
            nearpos = np.argsort(distance,axis=0)
            # Obtaining absolute value
            D = np.absolute(x[(n-1)+(m)*tao]-x[nearpos[1]+m*tao])
            # Obtaining square root
            R = np.sqrt(D**2+neardis[1]**2)
            if (D/neardis[1] > rtol or R/Ra > atol):
                FNN[m-1,[0]] = FNN[m-1,[0]]+1
                
        if ((FNN[m-1,0]/FNN[0,0])*100 < 0.01):
            emb_dim = m
            break
    
    # Calculating embedding dimension
    if emb_dim == 0:
        min_indexes = np.where(FNN[:,0] == min(FNN[:,0]))
        emb_dim = min_indexes[0][0]+1
        
    # Percentage of the FNN
    FNN = (FNN/FNN[0,0])*100
    
    return FNN, emb_dim

def psr_deneme(x, m, tao, npoint = None):
    # Phase space reconstruction
    # x : time series 
    # m : embedding dimension
    # tao : time delay
    # npoint : total number of reconstructed vectors
    # Y : M x m matrix
    
    N = len(x)
    
    if (npoint != None):
        M = npoint
    else:
        M = N - (m-1)*tao

    Y = np.zeros((M,m))
    
    
    for i in range(1,m+1):
        Y[:,[i-1]]=x[np.array([j for j in range(0, M)])  + (i-1)*tao]
    return Y

def meanperiod(x,Fs):
    # Estimates the mean period of the time series
    # x:time series
    # Fs:sampling frequency
    # Based upon the Matlab example obtained from here: 
    # http://www.mathworks.com/help/matlab/ref/fft.html
    # Band-Pass filter example was obtained from
    # http://www.mathworks.com/help/dsp/ref/fdesign.bandpass.html

    T = 1/Fs
    L = len(x)
    # t = (0:L-1)*T;

    NFFT = pow(2, math.ceil(math.log(L)/math.log(2))) # Next power of 2 from length of y
    X = np.fft.fft(x, n = NFFT, axis = 0)/L
    f = Fs/2*np.linspace(0, 1, num = (NFFT//2)+1)

    # Plot single-sided amplitude spectrum.
    # plot(f,2*abs(X(1:NFFT/2+1))) 
    # title('Single-Sided Amplitude Spectrum of y(t)')
    # xlabel('Frequency (Hz)')
    # ylabel('|Y(f)|')

    mean_period = 1/(np.sum(2*np.absolute(X[0:(NFFT//2)+1]))/len(X[0:(NFFT//2)+1]))
    
    return mean_period

def lyarosenstein(x, m, tao, fs, meanperiod, maxiter):
    # d:divergence of nearest trajectoires
    # x:signal
    # tao:time delay
    # m:embedding dimension
    # fs:sampling frequency
    
    N = len(x)
    M = N - (m-1)*tao
    Y = psr_deneme(x, m, tao)
    neardis = np.zeros(M)
    nearpos = np.zeros(M)
    d = np.zeros(maxiter)
    
    # Obtaining nearest distances
    for i in range(1, M+1):
        x0 = np.ones([M,1]) * Y[i-1, :]
        distance = np.sqrt(np.sum((Y-x0)**2,1))
        
        for j in range(1, M+1):
            if (np.absolute(j-i) <= meanperiod):
                distance[j-1] = 1e10
                
        neardis[i-1] = np.amin(distance)
        nearpos[i-1] = np.argmin(distance)
    
    # Obtaining log of divergence
    for k in range(1, maxiter+1):
        maxind = M-k
        evolve = 0
        pnt = 0
        
        for j in range(1, M+1):
            if (j <= maxind and (nearpos[j-1] + 1) <= maxind):
                dist_k = np.sqrt(np.sum((Y[j+k-1,:] - Y[int(nearpos[j-1]+k),:])**2,0))
                if (np.not_equal(dist_k, 0)):
                    evolve += np.log(dist_k)
                    pnt += 1
                    
        if (pnt > 0):
            d[k-1] = evolve/pnt
        else:
            d[k-1] = 0
        
    tlinear = np.array([i for i in range(20,91)])
    F = np.polyfit(tlinear, d[tlinear-1], 1)
    lle = F[0]*fs

    return lle

def corrdim(x, m, tau):
    # Correlation dimension based on Grassberger-Procaccia algorithm (1983)
    # dg :Correlation dimension
    # C_R :Vector of contribution of the points in the Radius R (size = (R_max-R_min/R_step)+1)
    # R :Vector of radius (same size as C_R)
    # x :Time series
    # m :Embedded dimension
    # tau :Time delay
    # R_initial :Initial radius of the n-dimensional sphere (i.e. 0.3)
    # R_step :Increments of the radius (i.e 0.1)
    # R_max : Max radius (-1 R_step) of the n-dimensional sphere
    
    N = len(x)
    M = N - (m-1) * tau
    # Attractor reconstruction
    Y = psr_deneme(x, m, tau)
    R_min = 1000
    R_max = 0
    # Distance vector between all the points size of N^2 - N
    dist_vec = np.zeros((1,int((N**2)-N)))[0]
    n = 1
    
    # Estimating R_min and R_max
    # For each point
    for i in range(1, len(Y)+1):
        # Compare with the other points
        for j in range(1, len(Y)+1):
            # If it's not the same point
            if (i != j): 
                R_estimate = LA.norm(Y[i-1,:]-Y[j-1,:])
                dist_vec[n-1] = R_estimate
                # Estimate min radius
                if (R_estimate < R_min): 
                    R_min = R_estimate
                # Estimate max radius
                if (R_estimate > R_max):
                    R_max = R_estimate
            n = n + 1
            
    # Range of the Radius ignore and 1% of the max radius
    R_range = 0.01*(R_max - R_min)
    R_max = R_max - R_range
    if R_min == 0:
        # Solve some cases of NaN in R and C_R vectors 
        R_min = 1.0000e-06
        
    # Calculate step of the radius increase in the linear zone of 10 divisions
    R_step = (R_max - R_min)/10
    R = R_min
    C_R = np.zeros((1,int(np.floor(((R_max-R_min)/R_step)+1))))[0]
    n = 1
    totalSum = 0
    
    # Obtaining C(R) vector
    # R_init tends to R_max in steps of R_step
    while (R <= R_max):
        # For each distance in the distance vector
        for i in range(1, len(dist_vec)+1):
            #If it's contained in the radius
            if (dist_vec[i-1] < R):
                # Add 1 to the counter
                totalSum += 1
                
        # C_R calculation 
        C_R[n-1] = ((2/(M*(M-1)))*totalSum) 
        # Next step for the Radius 
        R += R_step
        totalSum = 0
        n += 1
        
    # Calculate ln(C(R)) and ln(R)
    # TODO: Try to find a way to fix this
    R = np.arange(R_min, R_max + 0.00000000001, R_step)
    lnC_R = np.log(C_R)
    lnR = np.log(R)
    
    # Calculate dg
    coefficients = np.polyfit(lnR[1:len(lnR)-5],lnC_R[1:len(lnC_R)-5],1);
    dg = coefficients[0]
    
    return dg, C_R, R

def hurstexp(x):
# Computing of Hurst exponent
# x is the time series
    
    # Length of the time series
    N = len(x);
    # Maximal 2-factors divisions of the time series
    max_divisions = round(np.log(N)/np.log(2))
    lnH_n = np.zeros((1, max_divisions))[0]
    lnn = np.zeros((1, max_divisions))[0]
    
    # For each division obtain the rescaled range
    for k in range(1,max_divisions+1):
        n = round(N/(2**(k-1)))
        if (n == 1):
            break
            
        lnn[k-1] = np.log(n)
        # Divide the time series
        X = x[0:n]
        # Obtaining the mean of the new time series
        m = np.mean(X)
        # Adjusting the time series to the mean
        Y = X - m
        Z = np.zeros((1,n))[0]
        R = 0
        S = 0
        totalSum = 0
        # Computing the cumulative deviate series
        for t in range(1, n+1):
            for i in range(1, t+1):
                totalSum += Y[i-2]
            Z[t-1] = totalSum
            totalSum = 0
        # Obtaining the range of the deviate series
        R = np.ptp(Z)
        
        # Computing standard deviation
        for i in range(1,n+1):
            totalSum += ((X[i-2] - m)**2)
        
        S = np.sqrt((totalSum)/n);
        
        # Computing the results of rescaled range and store them
        if(S == 0 or R/S <= 0):
            lnH_n[k-1] = 0
        else:
            lnH_n[k-1] = np.log(R/S)
            
    coefficients = np.polyfit(lnn,lnH_n,1)
    H = coefficients[0]
    
    return H

timeSeries = (pd.read_csv(StringIO(sys.argv[1]), header = None).transpose()).to_numpy()
filteredTimeSeries = filterTimeSeries(timeSeries,1,5,10,10)
PIM, TauOpt = pim(filteredTimeSeries,80,200)
FNN, embDim = knn_deneme(filteredTimeSeries, TauOpt,20,15,5)
meanPeriod = meanperiod(filteredTimeSeries,30)
larLyapunovExp = lyarosenstein(filteredTimeSeries,embDim,TauOpt,30,1/meanPeriod,600)
dimCorr, C_R, R = corrdim(filteredTimeSeries,embDim,TauOpt)
hurstExp = hurstexp(filteredTimeSeries)

z = filteredTimeSeries[2*TauOpt:len(filteredTimeSeries),0]
x = filteredTimeSeries[TauOpt:len(filteredTimeSeries)-TauOpt,0]
y = filteredTimeSeries[0:len(filteredTimeSeries)-2*TauOpt,0]

print(json.dumps({'dim_corr': round(dimCorr,4), 'hurst_exp': round(hurstExp,4), 'lyapunov_exp': round(larLyapunovExp,4), 'attractor': json.dumps({'x': list(x), 'y': list(y), 'z': list(z)})}))