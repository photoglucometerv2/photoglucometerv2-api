import pandas as pd
from io import StringIO
import sys
import json
import collections

dataset = pd.read_csv(StringIO(sys.argv[1]))
X = dataset.iloc[:, :-1].values
y = dataset.iloc[:, -1].values

from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.25, random_state = 0)

from sklearn.preprocessing import StandardScaler
sc = StandardScaler()
X_train = sc.fit_transform(X_train)
X_test = sc.transform(X_test)

from sklearn.svm import SVC
classifier = SVC(kernel = 'linear', random_state = 0)
classifier.fit(X_train, y_train)

y_pred = classifier.predict(X_test)

from sklearn.metrics import confusion_matrix, accuracy_score
cm = confusion_matrix(y_test, y_pred, labels=[1, 2, 3])
accuracy = accuracy_score(y_test, y_pred)

counts = collections.Counter(y)
counts = [{'class': int(key), 'count': counts[key]} for key in counts]

print(json.dumps({'confussion_matrix': cm.tolist(), 'accuracy_score': round(accuracy, 4), 'test_set_size': y_pred.size, 'counts': counts}))