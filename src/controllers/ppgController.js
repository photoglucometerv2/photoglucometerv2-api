let { PythonShell } = require('python-shell')
const Ppg = require('../models/ppg');
const ppgSerializer = require('../serializers/ppgSerializer');

async function processTimeSeries(timeSeries, timeSeriesName, ppgId) {
    try {
        PythonShell.run('src\\controllers\\process.py', { args: [timeSeries] }, async (error, result) => {
            if (error) {
                console.log(error);

                await Ppg.query().findById(ppgId).patch({
                    [`processing_status_${timeSeriesName}`]: -1
                });
            } else {
                result = JSON.parse(result[0]);
                await Ppg.query().findById(ppgId).patch({
                    [`processing_status_${timeSeriesName}`]: 1,
                    [`dim_corr_${timeSeriesName}`]: result.dim_corr,
                    [`hurst_exp_${timeSeriesName}`]: result.hurst_exp,
                    [`lyapunov_exp_${timeSeriesName}`]: result.lyapunov_exp,
                    [`attractor_${timeSeriesName}`]: result.attractor
                });
            }
        });
    } catch (error) {
        console.log(error);
        Ppg.query().findById(ppgId).patch({
            [`processing_status_${timeSeriesName}`]: -1
        });
    }
}

exports.index = async (req, res) => {
    try {
        const ppgs = await Ppg.query().orderBy('id').page(req.query.page, req.query.pageSize);

        res.send({
            ppgs: ppgSerializer.collection(ppgs.results, 'PpgIndexResponse'),
            total: ppgs.total
        })
    } catch (error) {
        console.log(error);
        res.status(400).send({
            message: 'Error while retrieving ppgs.'
        });
    }
};

exports.create = async (req, res) => {

};

exports.store = async (req, res) => {
    try {
        const ppg = await Ppg.query().insert({
            ppg: req.body.ppg,
            ppg_alt: req.body.ppg_alt,
            ecg: req.body.ecg,
            bp_systolic: req.body.bp_systolic,
            bp_diastolic: req.body.bp_diastolic,
            true_gluco: req.body.true_gluco,
            subject_age: req.body.subject_age,
            subject_height: req.body.subject_height,
            subject_weight: req.body.subject_weight,
            subject_sex: req.body.subject_sex,
            subject_isdiabetic: req.body.subject_isdiabetic,
            subject_email: req.body.subject_email
        });

        res.status(201).send();

        processTimeSeries(ppg.ppg, 'ppg', ppg.id);
        processTimeSeries(ppg.ppg_alt, 'alt', ppg.id);
        processTimeSeries(ppg.ecg, 'ecg', ppg.id);
    } catch (error) {
        console.log(error);
        res.status(400).send({
            message: 'Error while storing ppg.'
        });
    }
};

exports.show = async (req, res) => {
    try {
        res.send(ppgSerializer.serialize(req.ppg, 'PpgShowResponse'));
    } catch (error) {
        console.log(error);
        res.status(400).send({
            message: 'Error while retrieving ppg.'
        });
    }
};

exports.edit = async (req, res) => {

};

exports.update = async (req, res) => {

};

exports.destroy = async (req, res) => {
    try {
        await req.ppg.$query().delete();

        res.send();
    } catch (error) {
        console.log(error);
        res.status(500).send({
            message: 'Error while removing ppg'
        });
    }
};

exports.process = async (req, res) => {
    try {
        await req.ppg.$query().patch({
            processing_status_ppg: 0,
            processing_status_alt: 0,
            processing_status_ecg: 0
        });

        res.status(200).send();

        processTimeSeries(req.ppg.ppg, 'ppg', req.ppg.id);
        processTimeSeries(req.ppg.ppg_alt, 'alt', req.ppg.id);
        processTimeSeries(req.ppg.ecg, 'ecg', req.ppg.id);
    } catch (error) {
        console.log(error);
        res.status(400).send({
            message: 'Error while processing ppg.'
        });
    }
};

exports.model = async (req, res) => {
    try {
        let ppgs = [];
        for (const id of req.body.ids) {
            const ppg = await Ppg.query().findById(id);
            ppgs.push(ppg);
        }

        let rows = ppgs.map((ppg) => {
            let modelClass;

            if (ppg.true_gluco < 95) {
                modelClass = 1;
            } else if (ppg.true_gluco >= 95 && ppg.true_gluco < 120) {
                modelClass = 2;
            } else if (ppg.true_gluco >= 120) {
                modelClass = 3;
            }

            return `${ppg.bp_systolic},${ppg.bp_diastolic},${ppg.dim_corr_ppg},${ppg.hurst_exp_ppg},${ppg.lyapunov_exp_ppg},${ppg.dim_corr_alt},${ppg.hurst_exp_alt},${ppg.lyapunov_exp_alt},${ppg.dim_corr_ecg},${ppg.hurst_exp_ecg},${ppg.lyapunov_exp_ecg},${modelClass}`
        }).join('\n');

        const csv = `bp_systolic,bp_diastolic,dim_corr_ppg,hurst_exp_ppg,lyapunov_exp_ppg,dim_corr_alt,hurst_exp_alt,lyapunov_exp_alt,dim_corr_ecg,hurst_exp_ecg,lyapunov_exp_ecg,class\n${rows}`;
        console.log(csv);
        PythonShell.run('src\\controllers\\model.py', { args: [csv] }, async (error, result) => {
            if (error) {
                throw error;
            } else {
                result = JSON.parse(result[0]);
                res.status(200).send({
                    ...result,
                    csv
                });
            }
        });
    } catch (error) {
        console.log(error);
        res.status(400).send({
            message: 'Error while processing ppg.'
        });
    }
};