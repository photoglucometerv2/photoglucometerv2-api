exports.serialize = (ppg, option) => {
    switch(option){
        case 'PpgIndexResponse':
            return {
                id: ppg.id,
                bp_systolic: ppg.bp_systolic,
                bp_diastolic: ppg.bp_diastolic,
                true_gluco: ppg.true_gluco,
                subject_age: ppg.subject_age,
                subject_height: ppg.subject_height,
                subject_weight: ppg.subject_weight,
                subject_sex: ppg.subject_sex,
                subject_isdiabetic: ppg.subject_isdiabetic,
                subject_email: ppg.subject_email,
                dim_corr_ppg: ppg.dim_corr_ppg,
                hurst_exp_ppg: ppg.hurst_exp_ppg,
                lyapunov_exp_ppg: ppg.lyapunov_exp_ppg,
                processing_status_ppg: ppg.processing_status_ppg,
                dim_corr_alt: ppg.dim_corr_alt,
                hurst_exp_alt: ppg.hurst_exp_alt,
                lyapunov_exp_alt: ppg.lyapunov_exp_alt,
                processing_status_alt: ppg.processing_status_alt,
                dim_corr_ecg: ppg.dim_corr_ecg,
                hurst_exp_ecg: ppg.hurst_exp_ecg,
                lyapunov_exp_ecg: ppg.lyapunov_exp_ecg,
                processing_status_ecg: ppg.processing_status_ecg
            };
        case 'PpgShowResponse':
            return {
                id: ppg.id,
                ppg: ppg.ppg,
                ppg_alt: ppg.ppg_alt,
                ecg: ppg.ecg,
                bp_systolic: ppg.bp_systolic,
                bp_diastolic: ppg.bp_diastolic,
                true_gluco: ppg.true_gluco,
                subject_age: ppg.subject_age,
                subject_height: ppg.subject_height,
                subject_weight: ppg.subject_weight,
                subject_sex: ppg.subject_sex,
                subject_isdiabetic: ppg.subject_isdiabetic,
                subject_email: ppg.subject_email,
                dim_corr_ppg: ppg.dim_corr_ppg,
                hurst_exp_ppg: ppg.hurst_exp_ppg,
                lyapunov_exp_ppg: ppg.lyapunov_exp_ppg,
                attractor_ppg: ppg.attractor_ppg,
                processing_status_ppg: ppg.processing_status_ppg,
                dim_corr_alt: ppg.dim_corr_alt,
                hurst_exp_alt: ppg.hurst_exp_alt,
                lyapunov_exp_alt: ppg.lyapunov_exp_alt,
                attractor_alt: ppg.attractor_alt,
                processing_status_alt: ppg.processing_status_alt,
                dim_corr_ecg: ppg.dim_corr_ecg,
                hurst_exp_ecg: ppg.hurst_exp_ecg,
                lyapunov_exp_ecg: ppg.lyapunov_exp_ecg,
                attractor_ecg: ppg.attractor_ecg,
                processing_status_ecg: ppg.processing_status_ecg
            };
        default:
            return {};
    }
};

exports.collection = (ppgCollection, option) => {
    return ppgCollection.map((ppg) => {
        return exports.serialize(ppg, option);
    });
}
