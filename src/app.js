const express = require('express');
const cors = require('cors')
require('./db/objection.js');
const ppgRouter = require('./routers/ppgRouter');

const app = express();
app.use(cors());

app.use(express.json());
app.use(ppgRouter);

module.exports = app;
