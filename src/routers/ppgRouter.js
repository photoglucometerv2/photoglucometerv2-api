const express = require('express');
const binder = require('./binder');
const Ppg = require('../models/ppg');
const ppgController = require('../controllers/ppgController');

const router = new express.Router();

router.param('ppg', binder('ppg', Ppg));

router.post('/ppgs', ppgController.store);
router.get('/ppgs', ppgController.index);
router.post('/ppgs/model', ppgController.model);
router.get('/ppgs/:ppg', ppgController.show);
router.delete('/ppgs/:ppg', ppgController.destroy);
router.patch('/ppgs/:ppg/process', ppgController.process);

module.exports = router;
